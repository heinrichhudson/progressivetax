﻿using Microsoft.EntityFrameworkCore;
using ProgressiveTax.DAL;

namespace ProgressiveTax.Models
{
    public class AppContext : DbContext
    {
        public AppContext(DbContextOptions<AppContext> options) 
            : base(options) { }
        public AppContext() { }

        public DbSet<PostalCodes> PostalCodes { get; set; }
        public DbSet<Type> Type { get; set; }
        public DbSet<FlatRate> FlatRate { get; set; }
        public DbSet<ProgressivTax> ProgressivTax { get; set; }
    }
}
