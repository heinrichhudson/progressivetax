﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProgressiveTax.Models;
using System.Data;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Globalization;

namespace ProgressiveTax.Controllers
{
    
    public class HomeController : Controller
    {
        //CHECK IF AREA CODE EXIST
        [HttpPost]
        public ActionResult PostalCode(Models.PostalCodes model)
        {
            string conn = @"Server=.\SQLExpress;AttachDbFilename=C:\NEW\SalaryTax.mdf;Database=SalaryTax;Trusted_Connection = Yes; ";
            string TypeDescription = "";

            using (SqlConnection connection = new SqlConnection(conn))
            {
                connection.Open();
                SqlCommand dCmd = new SqlCommand("spGetPostalCode", connection)
                   {
                         CommandType = CommandType.StoredProcedure
                   };
                dCmd.Parameters.Add(new SqlParameter("@PostalCode", model.PostalCode));
                SqlDataAdapter da = new SqlDataAdapter(dCmd);
                DataSet ds = new DataSet();
                ds.Clear();
                da.Fill(ds, "PostalCode");

                if (ds.Tables["PostalCode"].Rows.Count > 0)
                    {
                         MyGlobalVariables.postalID = ds.Tables["PostalCode"].Rows[0]["TypeID"].ToString();
                         TypeDescription = ds.Tables["PostalCode"].Rows[0]["TypeDesc"].ToString();
                    }
                else
                    {
                         ViewBag.Message = "Postal code does not exist.";
                         return View("Index");
                    }

                connection.Close();
                ViewBag.TypeDesc = "Tax Type: "+TypeDescription;
                return View();
            }
        }


        //CALCULATE SALARY TAX
        [HttpPost]
        public ActionResult Salary(Models.Salary salModel)
        {
            int PostalCodeID = Int32.Parse(MyGlobalVariables.postalID);
            string conn = @"Server=.\SQLExpress;AttachDbFilename=C:\NEW\SalaryTax.mdf;Database=SalaryTax;Trusted_Connection = Yes; ";
            
            //Values: FromValue, ToValue, Rates, FlatRate, FlatValue
            string[] Gross = new string [5];

            using (SqlConnection connection = new SqlConnection(conn))
            {
                connection.Open();
                SqlCommand dCmd = new SqlCommand("spGetSalaryList", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                dCmd.Parameters.Add(new SqlParameter("@TypeID", PostalCodeID));
                SqlDataAdapter da = new SqlDataAdapter(dCmd);
                DataSet ds = new DataSet();
                ds.Clear();
                da.Fill(ds, "ProgressiveTax_tbl");

                if (ds.Tables["ProgressiveTax_tbl"].Rows.Count > 0)
                {

                    //FLAT RATE AND VALUE GET
                    if (ds.Tables["ProgressiveTax_tbl"].Rows.Count == 1)
                    {
                        
                        if (PostalCodeID == 2)
                        {
                            string SalaryValue = salModel.GrossValue;
                            string FlatValue = ds.Tables["ProgressiveTax_tbl"].Rows[0]["FlatValue"].ToString();
                            FlatValueCalculation(FlatValue, SalaryValue);
                        }
                        if (PostalCodeID == 3)
                        {
                            string SalaryValue = salModel.GrossValue;
                            string FlatRate = ds.Tables["ProgressiveTax_tbl"].Rows[0]["FlatRate"].ToString();
                            FlatRateCalculation(FlatRate, SalaryValue);
                        }
                        ViewBag.TotVat = "Total Tax: R" + TotalTax;
                        return View("PostalCode");
                    }


                    //PROGRESSIVE TAX GET
                    int total_rows = ds.Tables["ProgressiveTax_tbl"].Rows.Count;
                    for (int i = 0; i <= total_rows; i++)
                    {
                        Gross[0] = ds.Tables["ProgressiveTax_tbl"].Rows[i]["FromValue"].ToString();
                        Gross[1] = ds.Tables["ProgressiveTax_tbl"].Rows[i]["ToValue"].ToString();
                        Gross[2] = ds.Tables["ProgressiveTax_tbl"].Rows[i]["Rate"].ToString();
                        string SalaryValue = salModel.GrossValue;
                        if (ToExit ==1)
                        {
                            ViewBag.TotVat = "Total Tax: R"+TotalTax;
                            return View("PostalCode");
                        }
                        CalculateTaxing(Gross[0], Gross[1], Gross[2], SalaryValue);
                    }
                }
                else
                {
                    ViewBag.Message = "Salary must be more than 0";
                    return View("PostalCode");
                }
                connection.Close();

                ViewBag.TotVat = TotalTax;
                return View("PostalCode");
            }
        }

        float TotalTax = 0;
        float PreviousToValue = 0;
        int ToExit = 0;

        //GLOBAL VARIABLES
        public static class MyGlobalVariables
        {
            public static string postalID { get; set; }
        }
     
        //PROGRESSIVE TAX CALCULATION
        public string CalculateTaxing(string FromValue, string ToValue, string Rate, string SalaryValue)
        { 
            
            float _FromValue = float.Parse(FromValue, CultureInfo.InvariantCulture.NumberFormat);
            float _ToValue = float.Parse(ToValue, CultureInfo.InvariantCulture.NumberFormat);
            float _Rate = float.Parse(Rate, CultureInfo.InvariantCulture.NumberFormat);
            float _SalaryValue = float.Parse(SalaryValue, CultureInfo.InvariantCulture.NumberFormat);

            float TaxVal;

            if (_SalaryValue >= _ToValue)
            {
                _ToValue = _ToValue - PreviousToValue;
                TaxVal = (_ToValue / 100) * _Rate;
                TotalTax += TaxVal;
            }
            PreviousToValue += _ToValue;

            if (_SalaryValue <= _ToValue)
            {
                float _ValueLeft = _SalaryValue - _FromValue;
                TaxVal = (_ValueLeft / 100) * _Rate;
                TotalTax += TaxVal;
                ToExit = 1;
            }
            return TotalTax.ToString();
        }


        //FLAT VALUE
        public string FlatValueCalculation(string flatValue, string SalaryValue)
        {
            float _FlatValue = float.Parse(flatValue, CultureInfo.InvariantCulture.NumberFormat);
            float _SalaryValue = float.Parse(SalaryValue, CultureInfo.InvariantCulture.NumberFormat);
            if (_SalaryValue > 200000)
            {
                TotalTax = (_SalaryValue / 100) * 5;
            }
            else
            {
                TotalTax = _FlatValue;
            }
            return TotalTax.ToString();
        }


        //FLATE RATE
        public string FlatRateCalculation(string flatRate, string SalaryValue)
        {
            float _FlatRate = float.Parse(flatRate, CultureInfo.InvariantCulture.NumberFormat);
            float _SalaryValue = float.Parse(SalaryValue, CultureInfo.InvariantCulture.NumberFormat);
            TotalTax = (_SalaryValue / 100) * _FlatRate;
            return TotalTax.ToString();
        }




        public IActionResult Index()
        {
            return View();
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
