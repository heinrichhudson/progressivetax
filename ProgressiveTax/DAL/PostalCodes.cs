﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProgressiveTax.DAL
{
    [Table("PostalCodes")]
    public class PostalCodes
    {
            [Key]
            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            [Display(Name = "Key")]
            public int PostalID { get; set; }

            [Column(TypeName = "varchar")]
            [Display(Name = "Postal code")]
            [MaxLength(50, ErrorMessage = "max 50 characters")]
            [Required(ErrorMessage = "Postal Code is required")]
            public string PostalCode { get; set; }

            [Display(Name = "Type id")]
            public int TypeID { get; set; }

    }
}
